import 'robertdudus-user-profile';
import { Component, ComponentInterface } from "@stencil/core";

@Component({
  tag: "ixt-connection",
  styleUrl: "connection.css",
  shadow: true
})
export class Connection implements ComponentInterface {
  render() {
    return (
      <div>
        <div>Connection: v1.0.0 using user profile v0.1.6</div>
        <user-profile api-user="http://api-user" user-id="Robert"></user-profile>
      </div>
    );
  }
}
